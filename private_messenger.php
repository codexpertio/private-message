<?php
/**
* Plugin Name: Private Messaging System
* Plugin URI: https://codexpert.io
* Description: Subscriber can secretly message to their admin.
* Version: 1.0.0
* Requires at least: 5.2
* Requires PHP: 7.2
* Author: Codexpert
* Author URI: https://codexpert.io
* License: GPL v2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html
* Update URI: https://
* Text Domain: my-basics-plugin
* Domain Path: /languages
*/

/**
 * if accessed directly, exit.
 */
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

// require( plugin_dir_path( __FILE__) . 'admin_dsh.php');
// 
 class Private_Messaging {
     function __construct(){
         add_action( 'wp_footer', [ $this, 'admin_message_viwe_from_subscriber' ] );
         add_action( 'admin_menu', [ $this, 'custom_admin_menu_function' ] );
         register_activation_hook( __FILE__, [ $this, 'message_db' ] );
         add_shortcode( 'form_shortcut',   [ $this,'form_submission' ] );
        //  add_action( 'init', [$this,'message_functionality_view' ] );
     }
     
     public function admin_message_viwe_from_subscriber() {
         echo "...........Meassages.........";
    }
      
     public function custom_admin_menu_function() {
         add_menu_page(
             'Secret Message system_title', //Page Title,
             'Message from Subscriber', // Menu Title,
             'edit_posts', // edit_posts
             'wp-admin', // menu_slug
             [ $this, 'message_functionality_view_from_user' ], // page_callback_function
             'dashicons-buddicons-pm', //dashicons
             10
         );
     }
        
     public function message_functionality_view_from_user(){
        //  echo "...........Meassages from subscriber<br> Help Please!!!!!!!.........";
        // do_shortcode( '[form_shortcut]' );

        global $wpdb;
        $record = $wpdb->get_results( "SELECT * FROM pm_private_messenger_table" );
        foreach( $record as $row )
        {  
             echo $row->id . ". &emsp; " .$row->name . " &emsp; " . $row->message . "<br>"."<br>" ;    
            //  echo $row->search_time_parameter ."<br>";
        
        }
        
         
     }
     

    //  db creation
    function message_db(){
        global $wpdb; 
        $db_table_name = $wpdb->prefix . 'private_messenger_table';  // table name
        $charset_collate = $wpdb->get_charset_collate();

        //Check to see if the table exists already, if not, then create it
        if($wpdb->get_var( "show tables like '$db_table_name'" ) != $db_table_name ) 
        {
            $sql = "CREATE TABLE $db_table_name (
                        id int(11) NOT NULL auto_increment,
                        name varchar(60) NOT NULL,
                        message varchar(1000) NOT NULL,
                        time varchar(200) NOT NULL,
                        -- mobileno varchar(10) NOT NULL,
                        receiver varchar(100) NOT NULL,
                        status varchar(100) NOT NULL,
                        UNIQUE KEY id (id)
                ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        add_option( 'test_db_version', $test_db_version );
        }   
    }  
    
    //form's shortcode.............................
    function form_submission(){
         ?>

<form action="" method="post">
    <label for="admin-name">Choose admin:</label><br>
    <select name="admin_name_selector" id="admin_name">
        <option value="admin1">Admin 1</option>
        <option value="admin2">Admin 2</option>
        <option value="admin3">Admin 3</option>
    </select>
    <label for="message_area">Write Message:</label><br>
    <textarea id="message_" name="message_" rows="6"></textarea>
    <input type="submit" value="Send Message" name="submission">
</form>
<?php

// global data
    $option = isset($_POST['admin_name_selector']) ? $_POST['admin_name_selector'] : false ;
    if( isset( $_POST['message_'] ) ) {
        if ( $option ) {
            // echo  "our admin name:.............". $option;
            $receiver = $option;
            
        }
        global $current_user;  //globally user object return
        wp_get_current_user();
        $name = $current_user->user_login;
        global $wpdb;
        $m = $_POST['message_'];
        $count = $wpdb->get_var( "SELECT COUNT(*) FROM pm_private_messenger_table" );
        $id = $count + 1;
        $time =  date("Y-m-d h:i:sa");
        echo  "our admin name:.............". $receiver;



//    echo  "our message:.............". $m;

    $wpdb->insert(
            'pm_private_messenger_table', 
            array(
            'id' => $id,
            'name' =>  $name,
            'message' => $m,
            'time' => $time,
            // 'mobileno' => '017',
            'receiver' => $receiver,
            'status' => 'yes'
                ),
                array( '%s', '%s', '%s', '%s', '%s', '%s' )
            );
        }
    }

}

$pm_object = new Private_Messaging();

?>